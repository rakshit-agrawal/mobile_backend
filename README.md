# Mobile backend

* This repo contains code samples for building small backend servers for mobile applications.
* These servers can be deployed both locally as well as on Google Cloud.

## Requirements

* Google App Engine SDK for Python [https://cloud.google.com/appengine/docs/standard/python/download](https://cloud.google.com/appengine/docs/standard/python/download)
